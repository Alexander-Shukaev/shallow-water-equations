\usemodule[tikz]
\usemodule[pgfplots]

\usetikzlibrary{decorations.pathreplacing}

\input title
\input cases
\input matrix
\input slide

\setvariables
  [this]
  [title={Shallow Water Equations: Flux-Based Wave Decomposition Solver},
   author={Alexander K. Shukaev},
   keyword={shallow water equations, bathymetry, topography, dam break,
            hyperbolic partial differential equation, conservation law, balance
            law, finite volume method, well-balanced method, flux, source term,
            wave, Riemann problem, Godunov's method}]

\setupinteraction
  [state=start,
   title={\getvariable{this}{title}},
   author={\getvariable{this}{author}},
   keyword={\getvariable{this}{keyword}}]

\setuppapersize
  [S8]
  [S8]

\setupbodyfont
  [18pt]

\setuplayout
  [width=middle,
   height=middle,
   header=0cm,
   footer=0cm,
   margin=0cm,
   backspace=1cm,
   topspace=1cm]

\setuptolerance
  [verytolerant,
   stretch]

% \setupinteractionscreen
  % [option=max]

\setupwhitespace
  [big]

\setuppagenumbering
  [state=stop]

\setuphead
  [section]
  [style=\bfa]

\setupcaption
  [figure]
  [numberstyle=\bf,
   way=bysection,
   numbersection=yes,
   prefix=yes,
   prefixsegments=chapter:section]

\setupformulas
  [numberstyle=\bf,
   way=bysection,
   numbersection=yes,
   prefix=yes,
   prefixsegments=chapter:section]

\setupenumerations
  [before={\blank[big]},
   after={\blank[big]},
   alternative=serried,
   width=broad,
   distance=0.5em,
   headstyle=\bf,
   titlestyle=\bf,
   way=bysection,
   numbersection=yes,
   prefix=yes,
   prefixsegments=chapter:section,
   conversion=numbers]

\definereferenceformat
  [insection]
  [text={Section }]

\definereferenceformat
  [formula]
  [left=(,
   right=)]

\definereferenceformat
  [figure]
  [text={Figure }]

\defineenumeration
  [definition]
  [text={Definition},
   title=yes,
   style=\it,
   list=all,
   listtext={Definition }]

% \showframe

\Title{Shallow Water Equations: \crlf Flux-Based Wave Decomposition Solver}
\Author{\getvariable{this}{author}}

% \placebookmarks
  % [section]

\starttext

% -----------------------------------------------------------------------------

\startslide

\placetitle

\def
  \figurewidth
  {26cm}

\def
  \figureheight
  {13cm}

\startalignment
  [middle]
\starttikzpicture
  \input figures/front.tikz
\stoptikzpicture
\stopalignment

\stopslide

% -----------------------------------------------------------------------------

\startslide

\placeslidetitle{The Linear Riemann Problem}

\switchtobodyfont
  [small]
\placefigure
  [force]
  [figure:riemann_problem_jump_propagation]
  {An example of the solution to the linear Riemann problem for $m = 2$}{
    \startalignment
      [middle]
    \starttikzpicture
      \input figures/linear_riemann_problem.tikz
    \stoptikzpicture
    \stopalignment
  }
\switchtobodyfont
  [global]

\placeformula
  [formula:riemann_problem_solution_1]
\startformula
  \vec{q}(x, t)
  =
  \vec{q}_{l}
  +
  \sum_{p:\ \lambda^{(p)} < x / t}
  \vec{\mathcal{W}}^{(p)}
\stopformula

\placeformula
  [formula:riemann_problem_solution_2]
\startformula
  \vec{q}(x, t)
  =
  \vec{q}_{r}
  -
  \sum_{p:\ \lambda^{(p)} > x / t}
  \vec{\mathcal{W}}^{(p)}
\stopformula

\stopslide

% -----------------------------------------------------------------------------

\startslide

\placeslidetitle{Godunov's Method}

\placeformula
  [formula:godunov_method]
\startformula
  \vec{Q}_{i}^{n + 1}
  =
  \vec{Q}_{i}^{n}
  -
  \frac{\Delta t}{\Delta x}
  \cdot
  \left(
    \vec{f}(\vec{\mathcal{Q}}_{i + 1 / 2}^{n})
    -
    \vec{f}(\vec{\mathcal{Q}}_{i - 1 / 2}^{n})
  \right),
\stopformula

where $\vec{\mathcal{Q}}_{i \pm 1 / 2}^{n}$ are the solutions to the left and
right Riemann problems (respective to the $i$-th grid cell) at the
corresponding grid cell interfaces $x_{i \pm 1 / 2}$.

\startalignment
  [middle]
  {\bf Key Facts to Consider}
\stopalignment

\startitemize
  \item
    nonlinear hyperbolic PDE $\Leftrightarrow$ nonlinear Riemann problem;

  \item
    full structure of the nonlinear Riemann problem (transonic waves) $=$
    expensive;

  \item
    little information (just one point) is needed in the end anyway.
\stopitemize

\startalignment
  [middle]
  {\bf What to do?}

  Seek for approximate solutions $\hat{\vec{\mathcal{Q}}}_{i \pm 1 / 2}^{n}$ to
  the nonlinear Riemann problems.

  {\bf How?}

  Linearize the Riemann problems locally by {\em quasilinear form} of
  differential conservation law:
\stopalignment

\placeformula
  [formula:quasi_linear_differential_conservation_law]
\startformula
  \frac{\partial \vec{q}}{\partial t}
  +
  \vec{f}'(\vec{q})
  \cdot
  \frac{\partial \vec{q}}{\partial x}
  =
  \vec{0}.
\stopformula

\stopslide

% -----------------------------------------------------------------------------

\startslide

\placeslidetitle{The Roe Solver: Idea}

Solve the left and right linearized Riemann problems for $\vec{\alpha}_{i \pm 1
/ 2}$ to determine waves $\vec{\mathcal{W}}_{i \pm 1 / 2}^{(p)}$:

\placeformula
  [formula:left_property_wave_decomposition]
\startformula
  \vec{Q}_{i}
  -
  \vec{Q}_{i - 1}
  =
  \sum_{p = 1}^{m}
  \alpha_{i - 1 / 2}^{(p)}
  \cdot
  \hat{\vec{r}}_{i - 1 / 2}^{(p)}
  =
  \sum_{p = 1}^{m}
  \vec{\mathcal{W}}_{i - 1 / 2}^{(p)},
\stopformula

\placeformula
  [formula:right_property_wave_decomposition]
\startformula
  \vec{Q}_{i + 1}
  -
  \vec{Q}_{i}
  =
  \sum_{p = 1}^{m}
  \alpha_{i + 1 / 2}^{(p)}
  \cdot
  \hat{\vec{r}}_{i + 1 / 2}^{(p)}
  =
  \sum_{p = 1}^{m}
  \vec{\mathcal{W}}_{i + 1 / 2}^{(p)}.
\stopformula

Using \formula[formula:riemann_problem_solution_1] and
\formula[formula:riemann_problem_solution_2] we can define

\placeformula
  [formula:left_numerical_flux_2]
\startformula
  \vec{f}(\vec{\mathcal{Q}}_{i - 1 / 2}^{n})
  =
  \vec{f}(\vec{Q}_{i})
  -
  \sum_{p = 1}^{m}
  (\hat{\lambda}_{i - 1 / 2}^{(p)})^{+}
  \cdot
  \vec{\mathcal{W}}_{i - 1 / 2}^{(p)},
\stopformula

\placeformula
  [formula:right_numerical_flux_1]
\startformula
  \vec{f}(\vec{\mathcal{Q}}_{i + 1 / 2}^{n})
  =
  \vec{f}(\vec{Q}_{i})
  +
  \sum_{p = 1}^{m}
  (\hat{\lambda}_{i + 1 / 2}^{(p)})^{-}
  \cdot
  \vec{\mathcal{W}}_{i + 1 / 2}^{(p)},
\stopformula

where waves $\vec{\mathcal{W}}_{i \pm 1 / 2}^{(p)}$ are propagating at the
corresponding constant speeds $\hat{\lambda}_{i \pm 1 / 2}^{(p)}$, which are
eigenvalues of averaged Jacobians $\hat{A}_{i \pm 1 / 2}$ respectively.

Plugging \formula[formula:left_numerical_flux_2] and
\formula[formula:right_numerical_flux_1] into \formula[formula:godunov_method]
we arrive to the classical Godunov's method implementation:

\placeformula
  [formula:classical_godunov_method]
\startformula
  \vec{Q}_{i}^{n + 1}
  =
  \vec{Q}_{i}^{n}
  -
  \frac{\Delta t}{\Delta x}
  \cdot
  \left(
    \sum_{p = 1}^{m}
    (\hat{\lambda}_{i + 1 / 2}^{(p)})^{-}
    \cdot
    \vec{\mathcal{W}}_{i + 1 / 2}^{(p)}
    +
    \sum_{p = 1}^{m}
    (\hat{\lambda}_{i - 1 / 2}^{(p)})^{+}
    \cdot
    \vec{\mathcal{W}}_{i - 1 / 2}^{(p)}
  \right).
\stopformula

\stopslide

% -----------------------------------------------------------------------------

\startslide

\placeslidetitle{The Roe Solver: Conservation}

\startalignment
  [middle]
  {\bf Is \formula[formula:classical_godunov_method] conservative with any
  choice of {\tf $\hat{A}_{i \pm 1 / 2}$}, if waves are calculated by
  \formula[formula:left_property_wave_decomposition] and
  \formula[formula:right_property_wave_decomposition]?}

  No.

  {\bf What does it mean to be conservative?}
\stopalignment

\placeformula
  [formula:left_net_flux_conservation]
\startformula
  \vec{f}(\vec{Q}_{i})
  -
  \vec{f}(\vec{Q}_{i - 1})
  =
  \sum_{p = 1}^{m}
  \hat{\lambda}_{i - 1 / 2}^{(p)}
  \cdot
  \vec{\mathcal{W}}_{i - 1 / 2}^{(p)}
\stopformula

\placeformula
  [formula:right_net_flux_conservation]
\startformula
  \vec{f}(\vec{Q}_{i + 1})
  -
  \vec{f}(\vec{Q}_{i})
  =
  \sum_{p = 1}^{m}
  \hat{\lambda}_{i + 1 / 2}^{(p)}
  \cdot
  \vec{\mathcal{W}}_{i + 1 / 2}^{(p)}
\stopformula

\startalignment
  [middle]
  {\bf So how to make \formula[formula:classical_godunov_method] conservative?}
\stopalignment

Utilizing \formula[formula:left_property_wave_decomposition] and
\formula[formula:right_property_wave_decomposition], we can immediately rewrite
\formula[formula:left_net_flux_conservation] and
\formula[formula:right_net_flux_conservation] as

\placeformula
  [formula:left_net_flux_conservation_jacobian]
\startformula
  \vec{f}(\vec{Q}_{i})
  -
  \vec{f}(\vec{Q}_{i - 1})
  =
  \hat{A}_{i - 1 / 2}
  \cdot
  (
    \vec{Q}_{i}
    -
    \vec{Q}_{i - 1}
  ),
\stopformula

\placeformula
  [formula:right_net_flux_conservation_jacobian]
\startformula
  \vec{f}(\vec{Q}_{i + 1})
  -
  \vec{f}(\vec{Q}_{i})
  =
  \hat{A}_{i + 1 / 2}
  \cdot
  (
    \vec{Q}_{i + 1}
    -
    \vec{Q}_{i}
  ),
\stopformula

what gives us the restrictions that have to be imposed on the averaged
Jacobians $\hat{A}_{i \pm 1 / 2}$ so that
\formula[formula:classical_godunov_method] is guaranteed to be conservative.

\stopslide

% -----------------------------------------------------------------------------

\startslide

\placeslidetitle{The Roe Solver: Consequences}

Substantial amount of effort has been put into defining Roe averaged Jacobians
having property \formula[formula:left_net_flux_conservation_jacobian] and
\formula[formula:right_net_flux_conservation_jacobian] for various nonlinear
problems such as {\em shallow water equations} or {\em Euler equations} for gas
dynamics. However, for some problems Roe averaged Jacobians either cannot be
easily computed or cannot be defined at all.

\startalignment
  [middle]
  {\bf Summary of Disadvantages}
\stopalignment

\startitemize
  \item
    not generic and inflexible: requires special linearization of Jacobians
    (which might be not available for certain problems) in order to be
    conservative;

  \item
    unclear how to incorporate the {\em source term} properly.
\stopitemize

\startalignment
  [middle]
  {\bf Can we do better?}

  Yes!
\stopalignment

\stopslide

% -----------------------------------------------------------------------------

\startslide

\placeslidetitle{The Flux-Based Wave Decomposition Solver: Idea}

Recall conservation conditions \formula[formula:left_net_flux_conservation] and
\formula[formula:right_net_flux_conservation], and let's rewrite them as

\placeformula
  [formula:left_net_flux_conservation_flux_wave]
\startformula
  \vec{f}(\vec{Q}_{i})
  -
  \vec{f}(\vec{Q}_{i - 1})
  =
  \sum_{p = 1}^{m}
  \hat{\lambda}_{i - 1 / 2}^{(p)}
  \cdot
  \vec{\mathcal{W}}_{i - 1 / 2}^{(p)}
  =
  \sum_{p = 1}^{m}
  \vec{\mathcal{Z}}_{i - 1 / 2}^{(p)},
\stopformula

\placeformula
  [formula:right_net_flux_conservation_flux_wave]
\startformula
  \vec{f}(\vec{Q}_{i + 1})
  -
  \vec{f}(\vec{Q}_{i})
  =
  \sum_{p = 1}^{m}
  \hat{\lambda}_{i + 1 / 2}^{(p)}
  \cdot
  \vec{\mathcal{W}}_{i + 1 / 2}^{(p)}
  =
  \sum_{p = 1}^{m}
  \vec{\mathcal{Z}}_{i + 1 / 2}^{(p)},
\stopformula

where $\vec{\mathcal{Z}}_{i \pm 1 / 2}^{(p)}$ are the so-called {\em flux waves}.

The important observation is that since $\vec{\mathcal{W}}_{i \pm 1 / 2}^{(p)}$
are eigenvectors of averaged Jacobians $\hat{A}_{i \pm 1 / 2}$,
$\vec{\mathcal{Z}}_{i \pm 1 / 2}^{(p)}$ are their eigenvectors too. As a
result, it turns out that, instead of decomposing the jump in $\vec{q}$ (see
\formula[formula:left_property_wave_decomposition] and
\formula[formula:right_property_wave_decomposition]), we can directly decompose
the jump in $\vec{f}(\vec{q})$ into the flux waves by solving

\placeformula
  [formula:left_flux_wave_decomposition]
\startformula
  \vec{f}(\vec{Q}_{i})
  -
  \vec{f}(\vec{Q}_{i - 1})
  =
  \sum_{p = 1}^{m}
  \beta_{i - 1 / 2}^{(p)}
  \cdot
  \hat{\vec{r}}_{i - 1 / 2}^{(p)}
  =
  \sum_{p = 1}^{m}
  \vec{\mathcal{Z}}_{i - 1 / 2}^{(p)},
\stopformula

\placeformula
  [formula:right_flux_wave_decomposition]
\startformula
  \vec{f}(\vec{Q}_{i + 1})
  -
  \vec{f}(\vec{Q}_{i})
  =
  \sum_{p = 1}^{m}
  \beta_{i + 1 / 2}^{(p)}
  \cdot
  \hat{\vec{r}}_{i + 1 / 2}^{(p)}
  =
  \sum_{p = 1}^{m}
  \vec{\mathcal{Z}}_{i + 1 / 2}^{(p)}
\stopformula

for $\vec{\beta}_{i \pm 1 / 2}$ to obtain $\vec{\mathcal{Z}}_{i \pm 1 /
2}^{(p)}$.

\stopslide

% -----------------------------------------------------------------------------

\startslide

\placeslidetitle{The Flux-Based Wave Decomposition Solver: Conservation}

Finally, if we define waves as

\placeformula
  [formula:left_property_wave_from_flux_wave]
\startformula
  \vec{\mathcal{W}}_{i - 1 / 2}^{(p)}
  =
  \frac{
    \vec{\mathcal{Z}}_{i - 1 / 2}^{(p)}
  }{
    \hat{\lambda}_{i - 1 / 2}^{(p)}
  },
\stopformula

\placeformula
  [formula:right_property_wave_from_flux_wave]
\startformula
  \vec{\mathcal{W}}_{i + 1 / 2}^{(p)}
  =
  \frac{
    \vec{\mathcal{Z}}_{i + 1 / 2}^{(p)}
  }{
    \hat{\lambda}_{i + 1 / 2}^{(p)}
  },
\stopformula

and plug \formula[formula:left_property_wave_from_flux_wave] and
\formula[formula:right_property_wave_from_flux_wave] into
\formula[formula:classical_godunov_method], we will obtain {\bf conservative}
finite volume method out-of-the-box.

\startalignment
  [middle]
  {\bf Is \formula[formula:classical_godunov_method] conservative with any
  choice of {\tf $\hat{A}_{i \pm 1 / 2}$}, if waves are calculated by
  \formula[formula:left_property_wave_from_flux_wave] and
  \formula[formula:right_property_wave_from_flux_wave]?}

  Indeed!

  {\bf What is the benefit?}
\stopalignment

\placeformula
  [formula:left_trivial_jacobian_average]
\startformula
  \hat{A}_{i - 1 / 2}
  =
  \vec{f}'
  \left(
    \frac{\vec{Q}_{i}
    +
    \vec{Q}_{i - 1}}{2}
  \right)
\stopformula

\placeformula
  [formula:right_trivial_jacobian_average]
\startformula
  \hat{A}_{i + 1 / 2}
  =
  \vec{f}'
  \left(
    \frac{\vec{Q}_{i + 1}
    +
    \vec{Q}_{i}}{2}
  \right)
\stopformula

\stopslide

% -----------------------------------------------------------------------------

\startslide

\placeslidetitle{The Flux-Based Wave Decomposition Solver: Implementation}

Of course explicitly using \formula[formula:left_property_wave_from_flux_wave]
and \formula[formula:right_property_wave_from_flux_wave] with
\formula[formula:classical_godunov_method] in practice is not a good idea. The
reason is that both \formula[formula:left_property_wave_from_flux_wave] and
\formula[formula:right_property_wave_from_flux_wave] can obviously exhibit
numerical instabilities when $\hat{\lambda}_{i \pm 1 / 2}^{(p)} \rightarrow 0$.

Much better approach is to turn \formula[formula:classical_godunov_method] into
the new update scheme which relies flux waves $\vec{\mathcal{Z}}_{i \pm 1 /
2}^{(p)}$ directly:

\placeformula
  [formula:novel_godunov_method]
\startformula
  \vec{Q}_{i}^{n + 1}
  =
  \vec{Q}_{i}^{n}
  -
  \frac{\Delta t}{\Delta x}
  \cdot
  \left(
    \sum_{p = 1}^{m}
    \left|
      \mfunction{sgn}^{-}(\hat{\lambda}_{i + 1 / 2}^{(p)})
    \right|
    \cdot
    \vec{\mathcal{Z}}_{i + 1 / 2}^{(p)}
    +
    \sum_{p = 1}^{m}
    \left|
      \mfunction{sgn}^{+}(\hat{\lambda}_{i - 1 / 2}^{(p)})
    \right|
    \cdot
    \vec{\mathcal{Z}}_{i - 1 / 2}^{(p)}
  \right),
\stopformula

where

\startformula
  \mfunction{sgn}^{+}(\lambda)
  =
  \max(\mfunction{sgn}(\lambda), 0),
\stopformula

\startformula
  \mfunction{sgn}^{-}(\lambda)
  =
  \min(\mfunction{sgn}(\lambda), 0).
\stopformula

\stopslide

% -----------------------------------------------------------------------------

\startslide

\placeslidetitle{Balance Law, Source Term, and Bathymetry}

The {\em differential balance law}

\placeformula
  [formula:differential_balance_law]
\startformula
  \frac{\partial}{\partial t}
  \vec{q}(x, t)
  +
  \frac{\partial}{\partial x}
  \vec{f}(\vec{q}(x, t))
  =
  \vec{\psi}(\vec{q}(x, t), x)
\stopformula

consists of a {\em differential conservation law} with a source term
$\vec{\psi}(\vec{q}(x, t), x)$ on the right-hand side.

Underwater {\em topography} is generally called {\em bathymetry} further
denoted by $B(x)$. The free surface of the fluid is then given by

\placeformula
  [formula:fluid_free_surface]
\startformula
  S(x, t)
  =
  h(x, t)
  +
  B(x).
\stopformula

Furthermore, $H(x)$ denotes the distance from bathymetry $B(x)$ to some
constant {\em reference level} $L$ of the fluid surface:

\placeformula
  [formula:reference_level]
\startformula
  L
  =
  H(x)
  +
  B(x).
\stopformula

The shallow water equations then take the form

\placeformula
  [formula:swe_with_bathymetry]
\startformula
  \frac{\partial}{\partial t}
  \startmatrix
    \NC h
    \NR % ---------------------------------------------------------------------
    \NC h
        \cdot
        u
    \NR % ---------------------------------------------------------------------
  \stopmatrix
  +
  \frac{\partial}{\partial x}
  \startmatrix
    \NC h
        \cdot
        u
    \NR % ---------------------------------------------------------------------
    \NC h
        \cdot
        u^{2}
        +
        \frac{1}{2}
        \cdot
        g
        \cdot h^{2}
    \NR % ---------------------------------------------------------------------
  \stopmatrix
  =
  \startmatrix
    \NC 0
    \NR % ---------------------------------------------------------------------
    \NC g
        \cdot
        h
        \cdot
        H'(x)
    \NR % ---------------------------------------------------------------------
  \stopmatrix
  =
  \startmatrix
    \NC 0
    \NR % ---------------------------------------------------------------------
    \NC -g
        \cdot
        h
        \cdot
        B'(x)
    \NR % ---------------------------------------------------------------------
  \stopmatrix,
\stopformula

where, according to \formula[formula:differential_balance_law],

\placeformula
  [formula:swe_source_term]
\startformula
  \vec{\psi}(\vec{q}, x)
  =
  \startmatrix
    \NC 0
    \NR % ---------------------------------------------------------------------
    \NC -g
        \cdot
        q^{(1)}
        \cdot
        B'(x)
    \NR % ---------------------------------------------------------------------
  \stopmatrix.
\stopformula

\stopslide

% -----------------------------------------------------------------------------

\startslide

\placeslidetitle{Balance Law, Source Term, and Bathymetry}

\def
  \figurewidth
  {26cm}

\def
  \figureheight
  {13cm}

\switchtobodyfont
  [small]
\placefigure
  [force]
  [figure:bathymetry]
  {Shallow water equations with bathymetry}{
    \startalignment
      [middle]
    \starttikzpicture
      \input figures/bathymetry.tikz
    \stoptikzpicture
    \stopalignment
  }
\switchtobodyfont
  [global]

\stopslide

% -----------------------------------------------------------------------------

\startslide

\placeslidetitle{The Flux-Based Wave Decomposition Solver: \crlf Source Term
                 Treatment}

\startalignment
  [middle]
  {\bf Which method is called \quotation{well-balanced}?}
\stopalignment

A method is called {\em well-balanced} if equilibrium initial data is exactly
preserved by the method. Furthermore, the method should also accurately resolve
solutions that are small deviations from equilibrium data.

Many numerical approaches have been studied to solve
\formula[formula:differential_balance_law] properly. However, they have hard
time dealing with the solutions that are close to equilibrium state, i.e.\ when

\placeformula
  [formula:close_to_equilibrium_state]
\startformula
  \frac{\partial}{\partial x}
  \vec{f}(\vec{q}(x, t))
  \approx
  \vec{\psi}(\vec{q}(x, t), x),
\stopformula

while each term separately is large.

The flux-based wave decomposition approach can incorporate left and right
source term averages $\vec{\Psi}_{i \pm 1 / 2}$ directly into
\formula[formula:left_flux_wave_decomposition] and
\formula[formula:right_flux_wave_decomposition] to yield

\placeformula
  [formula:left_flux_wave_decomposition_with_source]
\startformula
  \vec{f}(\vec{Q}_{i})
  -
  \vec{f}(\vec{Q}_{i - 1})
  -
  \Delta x
  \cdot
  \vec{\Psi}_{i - 1 / 2}
  =
  \sum_{p = 1}^{m}
  \vec{\mathcal{Z}}_{i - 1 / 2}^{(p)},
\stopformula

\placeformula
  [formula:right_flux_wave_decomposition_with_source]
\startformula
  \vec{f}(\vec{Q}_{i + 1})
  -
  \vec{f}(\vec{Q}_{i})
  -
  \Delta x \cdot \vec{\Psi}_{i + 1 / 2}
  =
  \sum_{p = 1}^{m}
  \vec{\mathcal{Z}}_{i + 1 / 2}^{(p)},
\stopformula

what makes the flux-based wave decomposition solver superior to others.

\stopslide

% -----------------------------------------------------------------------------

\startslide

\placeslidetitle{The Flux-Based Wave Decomposition Solver: \crlf Source Term
                 Treatment}

\startalignment
  [middle]
  {\bf Why \formula[formula:left_flux_wave_decomposition_with_source] and
  \formula[formula:right_flux_wave_decomposition_with_source] are superior to
  other approaches?}
\stopalignment

In contrast to the other methods, this approach is particularly attractive in
cases where the solution is close to the equilibrium state, i.e.\ when
\formula[formula:close_to_equilibrium_state] takes place. To understand why,
recall \formula[formula:close_to_equilibrium_state] and consider left and right
discretized versions of it:

\placeformula
  [formula:left_well_balanced_proof]
\startformula
  \frac{\vec{f}(\vec{Q}_{i})
  -
  \vec{f}(\vec{Q}_{i - 1})}{\Delta x}
  =
  \vec{\Psi}_{i - 1 / 2},
\stopformula

\placeformula
  [formula:right_well_balanced_proof]
\startformula
  \frac{\vec{f}(\vec{Q}_{i + 1})
  -
  \vec{f}(\vec{Q}_{i})}{\Delta x}
  =
  \vec{\Psi}_{i + 1 / 2}.
\stopformula

The left-hand sides of both
\formula[formula:left_flux_wave_decomposition_with_source] and
\formula[formula:right_flux_wave_decomposition_with_source] will be zero
respectively, and hence all the flux waves $\vec{\mathcal{Z}}_{i \pm 1 /
2}^{(p)}$ will have zero strength, which is the indication of numerical
equilibrium state satisfying \formula[formula:left_well_balanced_proof] and
\formula[formula:right_well_balanced_proof] being maintained exactly. As a
result, it turns out that the method can be {\bf well-balanced}. The only trick
is to choose an appropriate averaging scheme for the source term.

\stopslide

% -----------------------------------------------------------------------------

\startslide

\placeslidetitle{The Flux-Based Wave Decomposition Solver: \crlf Source Term
                 Treatment}

\startalignment
  [middle]
  {\bf So how to define averaged source terms {\tf $\vec{\Psi}_{i \pm 1 / 2}$}
  properly for shallow water equations?}
\stopalignment

The so-called {\em surface-at-rest} is a very important equilibrium case, and
it can be expressed as

\placeformula
  [formula:surface_at_rest_case]
\startformula
  \startcases
    \NC u_{e}
    \NC \equiv
        0
    \NR % ---------------------------------------------------------------------
    \NC h_{e}(x)
        +
        B(x)
        =
        S_{e}
    \NC \equiv
        \mfunction{const}
    \NR % ---------------------------------------------------------------------
  \stopcases.
\stopformula

The flux-based wave decomposition method is {\bf well-balanced} if left and
right source term averages $\vec{\Psi}_{i \pm 1 / 2}$ are chosen as follows:

\placeformula
  [formula:left_source_average]
\startformula
  \vec{\Psi}_{i - 1 / 2}
  =
  \startmatrix
    \NC 0
    \NR % ---------------------------------------------------------------------
    \NC -g
        \cdot
        \frac{h_{i} + h_{i - 1}}{2}
        \cdot
        \frac{B_{i} - B_{i - 1}}{\Delta x}
    \NR % ---------------------------------------------------------------------
  \stopmatrix,
\stopformula

\placeformula
  [formula:right_source_average]
\startformula
  \vec{\Psi}_{i + 1 / 2}
  =
  \startmatrix
    \NC 0
    \NR % ---------------------------------------------------------------------
    \NC -g
        \cdot
        \frac{h_{i + 1} + h_{i}}{2}
        \cdot
        \frac{B_{i + 1} - B_{i}}{\Delta x}
    \NR % ---------------------------------------------------------------------
  \stopmatrix.
\stopformula

\startalignment
  [middle]
  {\bf Why?}
\stopalignment

It can be verified by direct substitution that when
\formula[formula:surface_at_rest_case] takes place and source terms are chosen
as \formula[formula:left_source_average] and
\formula[formula:right_source_average], then both
\formula[formula:left_well_balanced_proof] and
\formula[formula:right_well_balanced_proof] are satisfied exactly.

\stopslide

% -----------------------------------------------------------------------------

\startslide

\placeslidetitle{The Flux-Based Wave Decomposition Solver: Conclusion}

\startalignment
  [middle]
  {\bf Summary of Advantages}
\stopalignment

\startitemize
  \item
    generic and flexible: can be applied to a wide variety of problems
    (including those where Roe averaged Jacobians are not available);

  \item
    source term is incorporated naturally yielding well-balanced numerical
    scheme (when source term averages are defined properly);

  \item
    naturally extends to spatially varying fluxes (something that was not
    covered, but something to keep in mind).
\stopitemize

Finally, the flux-based wave decomposition solver for the shallow water
equations is extensively used in tsunami simulation, an application where it is
particularly critical that the method is {\bf well-balanced}, so that small
perturbations around the {\em ocean-at-rest} state are accurately captured
since the magnitude of a tsunami wave is generally one meter or even less while
the bathymetry varies on the order of several kilometers.

\stopslide

% -----------------------------------------------------------------------------

\stoptext
