function [jf] = flux_jacobian(q, g)
  jf = [0,                                      1;
        g * q(1) - q(2) * q(2) / (q(1) * q(1)), 2 * q(2) / q(1)];
end
