function [f] = flux(q, g)
  f = [q(2);
       q(2) * q(2) / q(1) + 0.5 * g * q(1) * q(1)];
end
