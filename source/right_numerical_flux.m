function [F] = right_numerical_flux(Q, B, i, g, dx, dt)
  average_Q = 0.5 * (Q{i + 1} + Q{i});

  A = flux_jacobian(average_Q, g);

  [R, S] = eig(A);

  s = diag(S);

  courant_number = max(s) * dt / dx;

  assert(courant_number <= 1);

  b = R \ (flux(Q{i + 1}, g) - flux(Q{i}, g) - ...
           dx * right_source(Q, B, i, g, dx));

  Z = R .* repmat(b', length(b), 1);

  F = flux(Q{i}, g) + Z * abs(bsxfun(@min, sign(s), 0));

  % Correction
  % F = F + 0.5 * Z * sign(s) .* (ones(size(s)) - (dt / dx) * abs(s));
end
