function godunov_method(S, ...
                        B, ...
                        u, ...
                        g, ...
                        left_boundary_condition, ...
                        right_boundary_condition, ...
                        left_x, ...
                        right_x, ...
                        cell_count, ...
                        time_count, ...
                        time_count_per_plot, ...
                        dt)
  close all;

  % Initialize {{{
  % ---------------------------------------------------------------------------
  dx     = (right_x - left_x) / cell_count;
  cell_x = left_x - 0.5 * dx : dx : right_x + 0.5 * dx;

  Q      = cell(1, cell_count + 2);
  next_Q = cell(1, cell_count + 2);

  for i = 2 : cell_count + 1
    Q{i} = [S(cell_x(i)) - B(cell_x(i));
            (S(cell_x(i)) - B(cell_x(i))) * u(cell_x(i))];
  end

  B = B(cell_x);

  h = zeros(1, cell_count);
  u = zeros(1, cell_count);
  d = zeros(1, cell_count);

  f = figure('Name',        'Shallow Water Equations: Flux Wave Solver', ...
             'NumberTitle', 'off');
  % ---------------------------------------------------------------------------
  % }}}

  for n = 1 : time_count
    % Update boundary conditions {{{
    % -------------------------------------------------------------------------
    switch left_boundary_condition
      case BoundaryCondition.outflow
        Q{1} = Q{2};
      case BoundaryCondition.reflection
        Q{1} = [ Q{2}(1);
                -Q{2}(2)];
    end

    switch right_boundary_condition
      case BoundaryCondition.outflow
        Q{cell_count + 2} = Q{cell_count + 1};
      case BoundaryCondition.reflection
        Q{cell_count + 2} = [ Q{cell_count + 1}(1);
                             -Q{cell_count + 1}(2)];
    end
    % -------------------------------------------------------------------------
    % }}}

    % Plot {{{
    % -------------------------------------------------------------------------
    if (mod(n, time_count_per_plot) == 0)
      for i = 1 : cell_count
        h(i) = Q{i + 1}(1);
        d(i) = Q{i + 1}(2);
        u(i) = d(i) / h(i);
      end

      figure(f);

      plots(1) = subplot(3, 1, 1);
      plot(cell_x(2 : cell_count + 1), B(2 : cell_count + 1) + h, 'b-', ...
           cell_x(2 : cell_count + 1), B(2 : cell_count + 1),     'k-');
      legend('Surface', ...
             'Bathymetry', ...
             'Location', 'NorthEastOutside');

      plots(2) = subplot(3, 1, 2);
      plot(cell_x(2 : cell_count + 1), u, 'r-');
      legend('Velocity', ...
             'Location', 'NorthEastOutside');

      plots(3) = subplot(3, 1, 3);
      plot(cell_x(2 : cell_count + 1), d, 'g-');
      legend('Discharge', ...
             'Location', 'NorthEastOutside');

      % Align all subplots {{{
      % -----------------------------------------------------------------------
      % NOTE: Legend boxes inherently have different dimensions, and since they
      % share space with the corresponding subplots, it causes subplots to have
      % different dimensions too, which looks somewhat ugly. In order to fix
      % that, enforce certain dimensions on all subplots.
      % -----------------------------------------------------------------------
      for p = plots
        position    = get(p, 'Position');
        position(3) = 0.70;

        set(p, 'Position', position)
      end
      % -----------------------------------------------------------------------
      % }}}

      pause(0.001);
    end
    % -------------------------------------------------------------------------
    % }}}

    if n == 1
      input('Start?');
    end

    if ~ishandle(f)
      return;
    end

    % Upwind {{{
    % -------------------------------------------------------------------------
    for i = 2 : cell_count + 1
      left_F  = left_numerical_flux(Q, B, i, g, dx, dt);
      right_F = right_numerical_flux(Q, B, i, g, dx, dt);

      next_Q{i} = Q{i} - (dt / dx) * (right_F - left_F);
    end

    Q = next_Q;
    % -------------------------------------------------------------------------
    % }}}

    fprintf(1, '%g\n', n);
  end
end
