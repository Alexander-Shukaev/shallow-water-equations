function [p] = left_source(Q, B, i, g, dx)
  average_h    = 0.5 * (Q{i}(1) + Q{i - 1}(1));
  derivative_B = (B(i) - B(i - 1)) / dx;

  p = [ 0;
       -g * average_h * derivative_B];
end
