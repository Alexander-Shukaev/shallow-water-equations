function [y] = hump(x)
  y = zeros(size(x));

  for i = 1 : length(x)
    if x(i) > -1 && x(i) < 1
      y(i) = cos(0.5 * pi * x(i));
    else
      y(i) = 0;
    end
  end
end
