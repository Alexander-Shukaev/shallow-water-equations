classdef BoundaryCondition
  enumeration
    reflection, outflow
  end
end
